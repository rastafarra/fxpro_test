package com.fxpro;

import java.util.ArrayList;
import java.util.Arrays;

public class Calculator {
    private int i = 0;


    public long calculateWaterAmounts(int[] landscape) {
        if (!validate(landscape))
            return -1;

        ArrayList<ArrayList<Integer>> pits = new ArrayList<>();
        i = 0;

        while (i < landscape.length - 1) {
            ArrayList<Integer> down = searchPit(Direction.Down, landscape);
            ArrayList<Integer> up = searchPit(Direction.Up, landscape);

            ArrayList<Integer> pit = new ArrayList<>();
            if (0 != down.size() && 0 != up.size()) {
                pit.addAll(down);
                pit.addAll(up);

                pits.add(pit);
            }
        }

        return getAmounts(pits);
    }


    private boolean validate(int[] landscape) {
        return 32000 > landscape.length && Arrays.stream(landscape).noneMatch(i -> i < 0 || i > 32000);
    }


    private ArrayList<Integer> searchPit(Direction direction, int[] landscape) {
        ArrayList<Integer> pit = new ArrayList<>();

        while (i < landscape.length - 1 &&
            ((Direction.Down.equals(direction) && landscape[i] >= landscape[i + 1]) ||
                Direction.Up.equals(direction) && landscape[i] < landscape[i + 1])) {
            pit.add(landscape[i]);

            i++;
        }

        if (landscape.length > i) {
            pit.add(landscape[i]);
        }

        if (Direction.Down.equals(direction))
            i++;

        return pit;
    }


    private long getAmounts(ArrayList<ArrayList<Integer>> pits) {
        return pits.stream().map(p -> {
            int size = p.size();
            if (0 == size)
                return 0;

            int min = Math.min(p.get(0), p.get(size - 1));

            return p.stream().filter(item -> min >= item).reduce(0, (a, b) -> a + (min - b));
        }).reduce(0, (a, b) -> a + b);
    }
}
