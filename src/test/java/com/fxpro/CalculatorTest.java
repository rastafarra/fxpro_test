package com.fxpro;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculatorTest {
    @Test
    public void testCalculateWaterAmounts() {
        int[] landscape = {5, 2, 3, 4, 5, 4, 0, 3, 1};

        Calculator app = new Calculator();
        long amount = app.calculateWaterAmounts(landscape);

        assertEquals(9, amount);
    }


//         5
//        4 4
//       3   3
//      2     2
//     1       1
//    0         0
    // 0?
    @Test
    public void testСoncaveСurve() {
        int[] landscape = {0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0};

        Calculator app = new Calculator();
        long amount = app.calculateWaterAmounts(landscape);

        assertEquals(0, amount);
    }



//    5---------5
//     4-------4
//      3-----3
//       2---2
//        1-1
//         0
    // 25?
    @Test
    public void testCurvedСurve() {
        int[] landscape = {5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5};

        Calculator app = new Calculator();
        long amount = app.calculateWaterAmounts(landscape);

        assertEquals(25, amount);
    }

//    5---------5---------5
//     4-------4 4-------4
//      3-----3   3-----3
//       2---2     2---2
//        1-1       1-1
//         0         0
    // 50?
    @Test
    public void testCurvedСurve2() {
        int[] landscape = {5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5};

        Calculator app = new Calculator();
        long amount = app.calculateWaterAmounts(landscape);

        assertEquals(50, amount);
    }


//    5                 5
//     4-------4-------4
//      3-----3 3-----3
//       2---2   2---2
//        1-1     1-1
//         0       0
    // 32?
    @Test
    public void testCurvedСurve3() {
        int[] landscape = {5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5};

        Calculator app = new Calculator();
        long amount = app.calculateWaterAmounts(landscape);

        assertEquals(32, amount);
    }


    @Test
    public void testValidatorLen() {
        int[] landscape = new int[32001];

        Calculator app = new Calculator();
        long amount = app.calculateWaterAmounts(landscape);

        assertEquals(-1, amount);
    }


    @Test
    public void testValidatorItems() {
        int[] landscape = new int[32000];
        landscape[0] = 320001;

        Calculator app = new Calculator();
        long amount = app.calculateWaterAmounts(landscape);

        assertEquals(-1, amount);
    }
}
